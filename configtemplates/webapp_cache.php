<?php
echo <<<PHP
<?php
return [
    'caches' => [
        'Cache\Transient' => [
            'options' => [
                'writable' => {$this->caching}
            ]
        ],
        'Cache\Persistence' => [
            'options' => [
                'writable' => {$this->caching}
            ]
        ],
    ],
];
PHP;
