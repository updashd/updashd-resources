<?php
echo <<<PHP
<?php
return [
    'worker_config' => [
        'public_key_file' => '{$this->notifierPublicKey}'
    ]
];

PHP;
