<?php
echo <<<PHP
<?php
return [
    // These are various options for the listeners attached to the ModuleManager
    'module_listener_options' => [
        'config_cache_enabled' => {$this->caching},
        'module_map_cache_enabled' => {$this->caching},
    ]
];
PHP;
