<?php
echo <<<PHP
<?php
return [
    'predis' => 'tcp://{$this->redisHost}:{$this->redisPort}?read_write_timeout=0',
    'private_key_file' => '{$this->workerPrivateKey}',
    'zone' => '{$this->zone}'
];
PHP;
