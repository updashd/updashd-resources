<?php
echo <<<PHP
<?php
return [
    'zones' => [
        '{$this->zone}' => ['predis' => 'tcp://{$this->redisHost}:{$this->redisPort}?read_write_timeout=0'],
    ],
    'doctrine' => [
        'connection' => [
            'host' => '{$this->mysqlHost}',
            'port' => {$this->mysqlPort},
            'dbname' => '{$this->mysqlDataBase}',
            'user' => '{$this->mysqlUserName}',
            'password' => '{$this->mysqlPassword}',
        ]
    ]
];
PHP;
