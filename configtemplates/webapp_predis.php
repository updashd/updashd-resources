<?php
echo <<<PHP
<?php
return [
    'predis' => 'tcp://{$this->redisHost}:{$this->redisPort}'
];

PHP;
