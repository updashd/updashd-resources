<?php
echo <<<PHP
<?php
return [
    'notifier_config' => [
        'public_key_file' => '{$this->notifierPublicKey}'
    ]
];

PHP;
