<?php
echo <<<PHP
<?php
return [
    'doctrine' => [
        'log_queries' => {$this->debug},
        'options' => [
            'dev_mode' => {$this->debug},
        ],
        'connection' => [
            'host' => '{$this->mysqlHost}',
            'port' => '{$this->mysqlPort}',
            'dbname' => '{$this->mysqlDataBase}',
            'user' => '{$this->mysqlUserName}',
            'password' => '{$this->mysqlPassword}',
        ]
    ]
];

PHP;
