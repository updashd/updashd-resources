<?php
echo <<<PHP
<?php
return [
    'predis' => 'tcp://{$this->redisHost}:{$this->redisPort}?read_write_timeout=0',
    'private_key_file' => '{$this->notifierPrivateKey}',
    'doctrine' => [
        'connection' => [
            'host' => '{$this->mysqlHost}',
            'port' => '{$this->mysqlPort}',
            'dbname' => '{$this->mysqlDataBase}',
            'user' => '{$this->mysqlUserName}',
            'password' => '{$this->mysqlPassword}',
        ]
    ]
];

PHP;
