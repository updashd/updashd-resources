<?php
echo <<<PHP
<?php
return [
    'predis' => 'tcp://{$this->redisHost}:{$this->redisPort}?read_write_timeout=0',
    'zone' => '{$this->zone}'
];
PHP;
