<?php

class MediaWikiTableHelper {
    public function __construct () {

    }

    public function start () {
        $str = '{| class="wikitable"' . PHP_EOL;
        return $str;
    }

    public function addHeader (array $columns) {
        $str = $this->addRow($columns, true);
        return $str;
    }

    public function addRow (array $columns, $isHeader = false) {
        $str = '|-' . PHP_EOL;

        foreach ($columns as $column) {
            $str .= ($isHeader ? '! scope="col"' : '') . '| ' . $column . PHP_EOL;
        }

        return $str;
    }

    public function end () {
        $str = '|}' . PHP_EOL;

        return $str;
    }
}