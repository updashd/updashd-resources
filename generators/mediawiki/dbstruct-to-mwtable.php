<?php
require_once 'MediaWikiTableHelper.php';

define('CHECKMARK_SRC', '[[File:Checkmark.png]]');

$config = array(
    'dbname' => 'updashd',
    'user' => 'updashd',
    'password' => 'secretpassword',
    'host' => 'mysql.example.com',
    'port' => '3306'
);

$config = array_replace_recursive($config, include 'config.local.php');

class PDOConfig extends \PDO {
    public function __construct ($config) {
        $dns = 'mysql:dbname=' . $config['dbname'] . ";host=" . $config['host'];

        parent::__construct($dns, $config['user'], $config['password']);

        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
}

function printTitle ($title, $isSection = false) {
    $str = PHP_EOL;
    $str .= str_repeat('=', $isSection ? 2 : 3);
    $str .= ' ' . $title . ' ';
    $str .= str_repeat('=', $isSection ? 2 : 3);
    $str .= PHP_EOL;
    return $str;
}

$pdo = new PDOConfig($config);

$stmt = $pdo->prepare('SHOW TABLES');
$stmt->execute();
$tables = $stmt->fetchAll(PDO::FETCH_COLUMN);

$mwth = new MediaWikiTableHelper();

$fkeyStatement = $pdo->prepare("
    SELECT 
      TABLE_NAME, 
      COLUMN_NAME, 
      CONSTRAINT_NAME, 
      REFERENCED_TABLE_NAME, 
      REFERENCED_COLUMN_NAME
    FROM
      INFORMATION_SCHEMA.KEY_COLUMN_USAGE
    WHERE
      TABLE_SCHEMA = :database AND
      TABLE_NAME = :tablename AND
      CONSTRAINT_NAME != 'PRIMARY'
    ");

foreach ($tables as $table) {
    $descStmt = $pdo->prepare('DESCRIBE ' . $table);
    $descStmt->execute();
    $description = $descStmt->fetchAll(PDO::FETCH_ASSOC);

    $indexStmt = $pdo->prepare('SHOW INDEXES FROM ' . $table);
    $indexStmt->execute();
    $indexes = $indexStmt->fetchAll(PDO::FETCH_ASSOC);

    $fkeyStatement->execute(array('database' => $config['dbname'], 'tablename' => $table));
    $foreignKeys = $fkeyStatement->fetchAll(PDO::FETCH_ASSOC);

    echo printTitle($table, true);

    //////////////////////////////////////////////
    // Columns
    //////////////////////////////////////////////
    echo printTitle($table . ' Columns');
    echo $mwth->start();

    echo $mwth->addHeader(array(
        'Column Name',
        'Data Type',
        'Default',
        'Not Null',
        'Unsigned',
        'Description',
        'Example'
    ));

    foreach ($description as $descRow) {
        echo $mwth->addRow(array(
            $descRow['Field'],
            $descRow['Type'],
            stripos($descRow['Extra'], 'auto_increment') !== FALSE ? 'AUTO_INCREMENT' : $descRow['Default'], // Default
            $descRow['Null'] == 'YES' ? '' : CHECKMARK_SRC,
            stripos($descRow['Type'], 'unsigned') !== FALSE ? CHECKMARK_SRC : '',
            '',
            ''
        ));
    }

    echo $mwth->end();


    //////////////////////////////////////////////
    // Indexes
    //////////////////////////////////////////////
    echo printTitle($table . ' Indexes');
    echo $mwth->start();

    echo $mwth->addHeader(array(
        'Index Name',
        'Columns',
        'Type',
    ));

    $indexBuff = array();
    foreach ($indexes as $indexRow) {
        $indexBuff[$indexRow['Key_name']] = array(
            'Key_name' => $indexRow['Key_name'],
            'Column_name' =>
                isset($indexBuff[$indexRow['Key_name']])
                ? $indexBuff[$indexRow['Key_name']]['Column_name'] . ', ' . $indexRow['Column_name']
                : $indexRow['Column_name'],
            'Non_unique' => $indexRow['Non_unique']
        );
    }

    foreach ($indexBuff as $indexRow) {
        echo $mwth->addRow(array(
            $indexRow['Key_name'],
            $indexRow['Column_name'],
            $indexRow['Non_unique'] ? '' : 'UNIQUE',
        ));
    }

    echo $mwth->end();


    //////////////////////////////////////////////
    // Foreign Keys
    //////////////////////////////////////////////
    echo printTitle($table . ' Foreign Keys');
    echo $mwth->start();

    echo $mwth->addHeader(array(
        'Name',
        'Columns',
        'Referenced Table',
        'Referenced Columns',
        'On Update',
        'On Delete',
    ));

    foreach ($foreignKeys as $foreignKey) {
        echo $mwth->addRow(array(
            $foreignKey['CONSTRAINT_NAME'],
            $foreignKey['COLUMN_NAME'],
            $foreignKey['REFERENCED_TABLE_NAME'],
            $foreignKey['REFERENCED_COLUMN_NAME'],
            '', // TODO: Implement this
            '', // TODO: Implement this
        ));
    }

    echo $mwth->end();

}