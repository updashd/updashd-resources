<?php
$config = array(
    'dbname' => 'updashd',
    'user' => 'updashd',
    'password' => 'secretpassword',
    'host' => 'mysql.example.com',
    'port' => '3306'
);

$config = array_replace_recursive($config, include 'config.local.php');

class PDOConfig extends \PDO {
    public function __construct ($config) {
        $dns = 'mysql:dbname=' . $config['dbname'] . ';host=' . $config['host'] . ';port=' . $config['port'];

        parent::__construct($dns, $config['user'], $config['password']);

        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
}

$pdo = new \PDOConfig($config);

$tables = [
    'account',
    'account_notifier',
    'account_person',
    'environment',
    'incident',
    'incident_notifier_history',
    'metric_type',
    'node',
    'node_service',
    'node_service_notifier',
    'node_service_zone',
    'notifier',
    'person',
    'person_setting',
    'result',
    //'result_latest', // We skip this table, because it doesn't follow the standard primary key structure
    'result_metric',
    'service',
    'service_metric_field',
    'setting',
    'severity',
    'zone',
];

function getAuditTableName ($table) {
    return 'zz_audit_' . $table;
}

function getDeleteStatement ($table, $column, $value) {
    return str_replace([
        '__TABLE__',
        '__COLUMN__',
        '__VALUE__'
    ],[
        $table,
        $column,
        $value
    ],
    'DELETE FROM __TABLE__ WHERE __COLUMN__ = __VALUE__');
}

function getUpdateStatement ($table, $column, $oldId, $newId) {
    return str_replace([
        '__TABLE__',
        '__COLUMN__',
        '__NEW_ID__',
        '__OLD_ID__'
    ],[
        $table,
        $column,
        $newId,
        $oldId
    ],
    'UPDATE  __TABLE__ SET __COLUMN__ = __NEW_ID__ WHERE __COLUMN__ = __OLD_ID__');
}

function getEventInsertStatement ($type, $auditTable, $oldId = 'NULL', $newId = 'NULL') {
    return str_replace([
        '__AUDIT_TABLE__',
        '__EVENT_TYPE__',
        '__OLD_ID__',
        '__NEW_ID__'
    ],[
        $auditTable,
        $type,
        $oldId,
        $newId
    ],
    'INSERT INTO __AUDIT_TABLE__ (event_type, old_id, new_id, event_date) VALUES (\'__EVENT_TYPE__\', __OLD_ID__, __NEW_ID__, NOW())');
}

function getEventSubscriptionInsertSql ($type, $table) {
    return str_replace([
        '__EVENT_TYPE__',
        '__TABLE__'
    ],[
        $type,
        $table
    ],
    "
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = '__TABLE__' AND db_event_type = '__EVENT_TYPE__'
");
}

function getPrimaryKeyFieldType ($table) {
    global $pdo;
    global $config;

    $sql = "
        SELECT UPPER(COLUMN_TYPE) AS column_type
        FROM information_schema.COLUMNS
        WHERE COLUMN_KEY = 'PRI'  && TABLE_SCHEMA = :dbName && TABLE_NAME = :tableName
    ";

    $stmt = $pdo->prepare($sql);
    $stmt->execute([
        'dbName' => $config['dbname'],
        'tableName' => $table
    ]);

    return $stmt->fetchColumn(0);
}

function getForeignKeyHandlers ($table) {
    global $pdo;
    global $config;

    /*
      1 = ON DELETE CASCADE,
      2 = ON DELETE SET NULL,
      4 = ON UPDATE CASCADE,
      8 = ON UPDATE SET NULL,
      16 = ON DELETE NO ACTION,
      32 = ON UPDATE NO ACTION.
    */

    $sql = "
        SELECT
            src.*,
            CASE
                WHEN src.del = 1 THEN 'CASCADE'
                WHEN src.del = 2 THEN 'SET NULL'
                WHEN src.del = 16 THEN 'NO ACTION'
                ELSE 'RESTRICT'
            END AS del_ac,
            CASE
                WHEN src.del = 4 THEN 'CASCADE'
                WHEN src.del = 8 THEN 'SET NULL'
                WHEN src.del = 32 THEN 'NO ACTION'
                ELSE 'RESTRICT'
            END AS upd_ac
        FROM (
            SELECT
                isf.ID,
                isf.FOR_NAME,
                isf.REF_NAME,
                isf.N_COLS,
                isfc.FOR_COL_NAME,
                isfc.REF_COL_NAME,
                isfc.POS,
                TYPE & (1|2|16) as del,
                TYPE & (4|8|32) as upd
            FROM information_schema.INNODB_SYS_FOREIGN isf
            INNER JOIN information_schema.INNODB_SYS_FOREIGN_COLS isfc ON isf.ID = isfc.ID
            WHERE REF_NAME LIKE '" . $config['dbname'] . "/" . $table . "'
        ) AS src";

    $stmt = $pdo->prepare($sql);
    $stmt->execute();

    $result = [
        'update' => [],
        'delete' => []
    ];

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $onDelete = trim($row['del_ac']);
        $onUpdate = trim($row['upd_ac']);

//        list($idDb, $idKeyName) = explode('/', $row['ID']);

        // The foreign key that is referencing this table
        list($forDb, $forTable) = explode('/', $row['FOR_NAME']);

        if ($forTable == $table) {
            // We must do this otherwise MySQL has problems deleting records
            continue;
        }

        $forColName = $row['FOR_COL_NAME'];

        // The key that is being referenced
//        list($refDb, $refTable) = explode('/', $row['REF_NAME']);
        $refColName = $row['REF_COL_NAME'];

        // This effectively creates sql for handling ON UPDATE in a trigger
// I'm going to leave the ON UPDATE behavior for native.. I don't really care right now
//        switch ($onUpdate) {
//            case 'CASCADE':
//                $result['update'][] = getUpdateStatement($forTable, $forColName, 'OLD.' . $refColName, 'NEW.' . $refColName);
//                break;
//            case 'SET NULL':
//                $result['update'][] = getUpdateStatement($forTable, $forColName, 'OLD.' . $refColName, 'NULL');
//                break;
//            case 'RESTRICT':
//            case 'NO ACTION':
//            default: break;
//        }

        // This effectively creates sql for handling ON DELETE in a trigger
        switch ($onDelete) {
            case 'CASCADE':
                $result['delete'][] = getDeleteStatement($forTable, $forColName, 'OLD.' . $refColName);
                break;
            case 'SET NULL':
                $result['delete'][] = getUpdateStatement($forTable, $forColName, 'OLD.' . $refColName, 'NULL');
                break;
            case 'RESTRICT':
            case 'NO ACTION':
            default: break;
        }
    }

    return $result;
}

$structure = [];
foreach ($tables as $table) {
    $idField = $table . '_id';
    $auditTable = getAuditTableName($table);

    $structure[$table] = [
        'insert' => [
            getEventInsertStatement('I', $auditTable, 'NULL', 'NEW.' . $idField),
            getEventSubscriptionInsertSql('I', $table)
        ],
        'update' => [
            getEventInsertStatement('U', $auditTable, 'OLD.' . $idField, 'NEW.' . $idField),
            getEventSubscriptionInsertSql('U', $table)
        ],
        'delete' => [
            getEventInsertStatement('D', $auditTable, 'OLD.' . $idField, 'NULL'),
            getEventSubscriptionInsertSql('D', $table)
        ]
    ];

    $onTriggers = getForeignKeyHandlers($table);
    
    $structure[$table]['update'] = array_merge($structure[$table]['update'], $onTriggers['update']);
    $structure[$table]['delete'] = array_merge($structure[$table]['delete'], $onTriggers['delete']);
}

foreach ($structure as $table => $handlers) {
    $handlers = array_map(function($elem) { return array_map(function ($elem) { return '    ' . $elem . ";\n"; }, $elem); }, $handlers);
    $pkiType = getPrimaryKeyFieldType($table);
    $insertHandlerSql = implode('', $handlers['insert']);
    $updateHandlerSql = implode('', $handlers['update']);
    $deleteHandlerSql = implode('', $handlers['delete']);
    $auditTable = getAuditTableName($table);

    echo <<<SQL
-- Audit table for table: {$table}
DROP TABLE IF EXISTS {$auditTable};
CREATE TABLE {$auditTable} (
    event_id INT AUTO_INCREMENT PRIMARY KEY,
	event_type CHAR DEFAULT 'I' NOT NULL COMMENT 'I - Insert, U - Update, D - Delete',
	old_id {$pkiType} NULL,
	new_id {$pkiType} NULL,
	event_date DATETIME NULL
);
 
-- Triggers for table: {$table}
DROP TRIGGER IF EXISTS after_{$table}_insert;
CREATE TRIGGER after_{$table}_insert AFTER INSERT
ON {$table}
FOR EACH ROW
BEGIN
{$insertHandlerSql}
END;
 
DROP TRIGGER IF EXISTS before_{$table}_update;
CREATE TRIGGER before_{$table}_update BEFORE UPDATE
ON {$table}
FOR EACH ROW
BEGIN
{$updateHandlerSql}
END;
 
DROP TRIGGER IF EXISTS before_{$table}_delete;
CREATE TRIGGER before_{$table}_delete BEFORE DELETE
ON {$table}
FOR EACH ROW
BEGIN
{$deleteHandlerSql}
END;



SQL;
}
