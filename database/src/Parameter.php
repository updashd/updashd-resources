<?php
class Parameter {
    const TYPE_FLAG = 0;
    const TYPE_FLAG_VALUE = 1;
    const TYPE_FLAG_ARRAY = 2;
    const TYPE_STRING = 3;
    const TYPE_ARRAY = 4;
    
    const SEPARATOR_SPACE = ' ';
    const SEPARATOR_EQUAL = '=';
    const SEPARATOR_NONE = '';
    
    private $flagName;
    private $value;
    private $type;
    private $glue;
    private $separator;
    
    public function __construct ($flagName, $value = null, $type = self::TYPE_FLAG, $separator = self::SEPARATOR_SPACE) {
        $this->setFlagName($flagName);
        $this->setType($type);
        $this->setValue($value);
        $this->setSeparator($separator);
    }
    
    public function getParameterString () {
        $type = $this->getType();
        $value = $this->getValue();
    
        // Escape the argument if it has ", ', or space.
        if (is_string($value) && preg_match('[\"\' ]', $value)) {
            $value = escapeshellarg($value);
        }
    
        switch ($type) {
            case self::TYPE_FLAG:
                return $this->getFlagName();
                break;
            case self::TYPE_FLAG_VALUE:
                return $this->getFlagName() . $this->getSeparator() . $this->getValue();
                break;
            case self::TYPE_FLAG_ARRAY:
                return $this->getFlagName() . $this->getSeparator() . implode($this->getGlue(), $this->getValue());
                break;
            case self::TYPE_STRING:
                return $value;
                break;
            case self::TYPE_ARRAY:
                return implode($this->getGlue(), $this->getValue());
                break;
            default:
                throw new \Exception('Unknown parameter type');
                break;
        }
        
    }
    
    public function __toString () {
        return $this->getParameterString();
    }
    
    /**
     * @return mixed
     */
    public function getFlagName () {
        return $this->flagName;
    }
    
    /**
     * @param mixed $flagName
     */
    public function setFlagName ($flagName) {
        $this->flagName = $flagName;
    }
    
    /**
     * @return mixed
     */
    public function getValue () {
        return $this->value;
    }
    
    /**
     * @param mixed $value
     */
    public function setValue ($value) {
        $this->value = $value;
    }
    
    /**
     * @return mixed
     */
    public function getType () {
        return $this->type;
    }
    
    /**
     * @param mixed $type
     */
    public function setType ($type) {
        $this->type = $type;
    }
    
    /**
     * @return mixed
     */
    public function getGlue () {
        return $this->glue;
    }
    
    /**
     * @param mixed $glue
     */
    public function setGlue ($glue) {
        $this->glue = $glue;
    }
    
    /**
     * @return mixed
     */
    public function getSeparator () {
        return $this->separator;
    }
    
    /**
     * @param mixed $separator
     */
    public function setSeparator ($separator) {
        $this->separator = $separator;
    }
}