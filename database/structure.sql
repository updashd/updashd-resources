-- MySQL dump 10.13  Distrib 5.7.16, for Win64 (x86_64)
--
-- Host: localhost    Database: updashdtwo
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `account_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `owner_id` int(11) unsigned DEFAULT NULL,
  `updater_id` int(11) unsigned DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `creator_id` int(11) unsigned DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`account_id`),
  KEY `account_name` (`name`),
  KEY `account_updater_id` (`updater_id`),
  KEY `account_creator_id` (`creator_id`),
  KEY `account_owner_id_idx` (`owner_id`),
  CONSTRAINT `account_creator_id` FOREIGN KEY (`creator_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `account_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `account_updater_id` FOREIGN KEY (`updater_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `account_notifier`
--

DROP TABLE IF EXISTS `account_notifier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_notifier` (
  `account_notifier_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `account_notifier_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `account_id` int(11) unsigned NOT NULL,
  `notifier_id` int(11) unsigned NOT NULL,
  `is_enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `is_default` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `sort_order` int(11) unsigned NOT NULL DEFAULT '0',
  `frequency` int(11) unsigned NOT NULL DEFAULT '3600' COMMENT 'Max frequency a notice can be sent in seconds.',
  `config` longtext COLLATE utf8mb4_unicode_ci,
  `updater_id` int(11) unsigned DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `creator_id` int(11) unsigned DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`account_notifier_id`),
  KEY `ac_account_id` (`account_id`),
  KEY `ac_sort_order` (`sort_order`),
  KEY `ac_notifier_id` (`notifier_id`),
  KEY `ac_updater_id` (`updater_id`),
  KEY `ac_creator_id` (`creator_id`),
  CONSTRAINT `ac_account_id` FOREIGN KEY (`account_id`) REFERENCES `account` (`account_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ac_creator_id` FOREIGN KEY (`creator_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `ac_notifier_id` FOREIGN KEY (`notifier_id`) REFERENCES `notifier` (`notifier_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ac_updater_id` FOREIGN KEY (`updater_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `account_person`
--

DROP TABLE IF EXISTS `account_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_person` (
  `account_person_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(11) unsigned NOT NULL,
  `person_id` int(11) unsigned NOT NULL,
  `role_type` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'VIEWER',
  `updater_id` int(11) unsigned DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `creator_id` int(11) unsigned DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`account_person_id`),
  UNIQUE KEY `uniq_account_person_id` (`account_id`,`person_id`),
  KEY `account_person_creator_id` (`creator_id`),
  KEY `account_person_updater_id` (`updater_id`),
  KEY `account_person_person_id_idx` (`person_id`),
  CONSTRAINT `account_person_account_id` FOREIGN KEY (`account_id`) REFERENCES `account` (`account_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `account_person_creator_id` FOREIGN KEY (`creator_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `account_person_person_id` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `account_person_updater_id` FOREIGN KEY (`updater_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `environment`
--

DROP TABLE IF EXISTS `environment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `environment` (
  `environment_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `environment_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `account_id` int(11) unsigned NOT NULL,
  `bootstrap_color` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'info',
  `layout_color` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'aqua',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `updater_id` int(11) unsigned DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `creator_id` int(11) unsigned DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`environment_id`),
  KEY `environment_name` (`environment_name`),
  KEY `environment_account_id` (`account_id`),
  KEY `environment_updater_id` (`updater_id`),
  KEY `environment_creator_id` (`creator_id`),
  CONSTRAINT `environment_account_id` FOREIGN KEY (`account_id`) REFERENCES `account` (`account_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `environment_creator_id` FOREIGN KEY (`creator_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `environment_updater_id` FOREIGN KEY (`updater_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `incident`
--

DROP TABLE IF EXISTS `incident`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `incident` (
  `incident_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `node_service_id` int(11) unsigned NOT NULL,
  `zone_id` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_first_seen` datetime NOT NULL,
  `date_last_seen` datetime NOT NULL,
  `severity` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ERR',
  `account_id` int(11) unsigned NOT NULL,
  `message_code` int(11) DEFAULT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `updater_id` int(11) unsigned DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `creator_id` int(11) unsigned DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `is_resolved` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`incident_id`),
  UNIQUE KEY `uniq_incident_occurrence` (`node_service_id`,`zone_id`,`date_first_seen`),
  KEY `incident_severity` (`severity`(1)),
  KEY `incident_date` (`date_first_seen`),
  KEY `incident_node_service_zone` (`node_service_id`,`zone_id`),
  KEY `incident_updater_id` (`updater_id`),
  KEY `incident_creator_id` (`creator_id`),
  KEY `incident_zone_id` (`zone_id`),
  KEY `incident_account_id` (`account_id`),
  KEY `incident_severity_severity_id_fk` (`severity`),
  CONSTRAINT `incident_account_id` FOREIGN KEY (`account_id`) REFERENCES `account` (`account_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `incident_creator_id` FOREIGN KEY (`creator_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `incident_node_service_id` FOREIGN KEY (`node_service_id`) REFERENCES `node_service` (`node_service_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `incident_severity_severity_id_fk` FOREIGN KEY (`severity`) REFERENCES `severity` (`severity_id`) ON UPDATE CASCADE,
  CONSTRAINT `incident_updater_id` FOREIGN KEY (`updater_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `incident_zone_id` FOREIGN KEY (`zone_id`) REFERENCES `zone` (`zone_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `incident_notifier_history`
--

DROP TABLE IF EXISTS `incident_notifier_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `incident_notifier_history` (
  `incident_notifier_history_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `incident_id` int(11) unsigned NOT NULL,
  `account_notifier_id` int(11) unsigned NOT NULL,
  `last_notice` datetime NOT NULL,
  `updater_id` int(11) unsigned DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `creator_id` int(11) unsigned DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`incident_notifier_history_id`),
  UNIQUE KEY `uniq_inh_incident_account_notifier` (`incident_id`,`account_notifier_id`),
  KEY `inh_creator_id` (`creator_id`),
  KEY `inh_updater_id` (`updater_id`),
  KEY `inh_account_notifier_id` (`account_notifier_id`),
  CONSTRAINT `inh_account_notifier_id` FOREIGN KEY (`account_notifier_id`) REFERENCES `account_notifier` (`account_notifier_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `inh_creator_id` FOREIGN KEY (`creator_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `inh_incident_id` FOREIGN KEY (`incident_id`) REFERENCES `incident` (`incident_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `inh_updater_id` FOREIGN KEY (`updater_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `metric_type`
--

DROP TABLE IF EXISTS `metric_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metric_type` (
  `metric_type_id` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `metric_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creator_id` int(11) unsigned DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updater_id` int(11) unsigned DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`metric_type_id`),
  KEY `metric_type_id` (`metric_type_id`),
  KEY `metric_type_creator_id` (`creator_id`),
  KEY `metric_type_updater_id` (`updater_id`),
  CONSTRAINT `metric_type_creator_id` FOREIGN KEY (`creator_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `metric_type_updater_id` FOREIGN KEY (`updater_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `node`
--

DROP TABLE IF EXISTS `node`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `node` (
  `node_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `node_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `environment_id` int(11) unsigned NOT NULL,
  `hostname` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ip` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `is_enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `sort_order` int(11) unsigned NOT NULL DEFAULT '0',
  `account_id` int(11) unsigned NOT NULL,
  `updater_id` int(11) unsigned DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `creator_id` int(11) unsigned DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`node_id`),
  KEY `node_name` (`node_name`),
  KEY `node_environment_id` (`environment_id`),
  KEY `node_sort_order` (`sort_order`),
  KEY `node_account_id` (`account_id`),
  KEY `node_account_environment` (`environment_id`,`account_id`),
  KEY `node_updater_id` (`updater_id`),
  KEY `node_creator_id` (`creator_id`),
  CONSTRAINT `node_account_id` FOREIGN KEY (`account_id`) REFERENCES `account` (`account_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `node_creator_id` FOREIGN KEY (`creator_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `node_environment_id` FOREIGN KEY (`environment_id`) REFERENCES `environment` (`environment_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `node_updater_id` FOREIGN KEY (`updater_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `node_service`
--

DROP TABLE IF EXISTS `node_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `node_service` (
  `node_service_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `node_service_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `node_id` int(11) unsigned NOT NULL,
  `service_id` int(11) unsigned NOT NULL,
  `is_enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `sort_order` int(11) unsigned NOT NULL,
  `schedule_type` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT 'once',
  `schedule_value` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'once',
  `config` longtext COLLATE utf8mb4_unicode_ci,
  `color` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'FF0000' COMMENT 'Hex Encoded, support for alpha if wanted',
  `updater_id` int(11) unsigned DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `creator_id` int(11) unsigned DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`node_service_id`),
  KEY `node_service_node_id` (`node_id`),
  KEY `node_service_service` (`service_id`),
  KEY `ns_node_service` (`node_id`,`service_id`),
  KEY `ns_node_service_enabled` (`node_id`,`service_id`,`is_enabled`),
  KEY `ns_updater_id` (`updater_id`),
  KEY `ns_creator_id` (`creator_id`),
  CONSTRAINT `ns_creator_id` FOREIGN KEY (`creator_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `ns_node_id` FOREIGN KEY (`node_id`) REFERENCES `node` (`node_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ns_service_id` FOREIGN KEY (`service_id`) REFERENCES `service` (`service_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ns_updater_id` FOREIGN KEY (`updater_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `node_service_notifier`
--

DROP TABLE IF EXISTS `node_service_notifier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `node_service_notifier` (
  `node_service_notifier_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `node_service_id` int(11) unsigned NOT NULL,
  `account_notifier_id` int(11) unsigned NOT NULL,
  `updater_id` int(11) unsigned DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `creator_id` int(11) unsigned DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`node_service_notifier_id`),
  UNIQUE KEY `uniq_node_service_notifier` (`node_service_id`,`account_notifier_id`),
  KEY `nsn_node_service_id` (`node_service_id`),
  KEY `nsn_zone_id` (`account_notifier_id`),
  KEY `nsn_updater_id` (`updater_id`),
  KEY `nsn_creator_id` (`creator_id`),
  CONSTRAINT `nsn_creator_id` FOREIGN KEY (`creator_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `nsn_node_service_id` FOREIGN KEY (`node_service_id`) REFERENCES `node_service` (`node_service_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `nsn_notifier_id` FOREIGN KEY (`account_notifier_id`) REFERENCES `account_notifier` (`account_notifier_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `nsn_updater_id` FOREIGN KEY (`updater_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `node_service_zone`
--

DROP TABLE IF EXISTS `node_service_zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `node_service_zone` (
  `node_service_zone_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `node_service_id` int(11) unsigned NOT NULL,
  `zone_id` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updater_id` int(11) unsigned DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `creator_id` int(11) unsigned DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`node_service_zone_id`),
  UNIQUE KEY `uniq_node_service_zone` (`node_service_id`,`zone_id`),
  KEY `nsz_node_service_id` (`node_service_id`),
  KEY `nsz_zone_id` (`zone_id`),
  KEY `nsz_updater_id` (`updater_id`),
  KEY `nsz_creator_id` (`creator_id`),
  CONSTRAINT `nsz_creator_id` FOREIGN KEY (`creator_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `nsz_node_service_id` FOREIGN KEY (`node_service_id`) REFERENCES `node_service` (`node_service_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `nsz_updater_id` FOREIGN KEY (`updater_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `nsz_zone_id` FOREIGN KEY (`zone_id`) REFERENCES `zone` (`zone_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notifier`
--

DROP TABLE IF EXISTS `notifier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifier` (
  `notifier_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `notifier_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `module_name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `account_id` int(11) unsigned DEFAULT NULL,
  `updater_id` int(11) unsigned DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `creator_id` int(11) unsigned DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`notifier_id`),
  KEY `notifier_sort_order_index` (`sort_order`),
  KEY `notifier_module_name_index` (`module_name`),
  KEY `notifier_notifier_name_index` (`notifier_name`),
  KEY `notifier_account_account_id_fk` (`account_id`),
  KEY `notifier_updater_id_fk` (`updater_id`),
  KEY `notifier_creator_id_fk` (`creator_id`),
  CONSTRAINT `notifier_account_account_id_fk` FOREIGN KEY (`account_id`) REFERENCES `account` (`account_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `notifier_creator_id_fk` FOREIGN KEY (`creator_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `notifier_updater_id_fk` FOREIGN KEY (`updater_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `person_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified` tinyint(1) NOT NULL DEFAULT '0',
  `is_blocked` tinyint(1) NOT NULL DEFAULT '0',
  `last_login` datetime DEFAULT NULL,
  `updater_id` int(11) unsigned DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `creator_id` int(11) unsigned DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `profile_picture_url` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_type` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'MEMBER',
  PRIMARY KEY (`person_id`),
  UNIQUE KEY `uniq_username` (`username`),
  UNIQUE KEY `uniq_email` (`email`),
  KEY `person_name` (`name`),
  KEY `person_updater_id` (`updater_id`),
  KEY `person_creator_id` (`creator_id`),
  CONSTRAINT `person_creator_id` FOREIGN KEY (`creator_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `person_updater_id` FOREIGN KEY (`updater_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `person_setting`
--

DROP TABLE IF EXISTS `person_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_setting` (
  `person_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) unsigned NOT NULL,
  `setting_id` int(11) unsigned NOT NULL,
  `setting_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creator_id` int(11) unsigned DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updater_id` int(11) unsigned DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`person_setting_id`),
  KEY `person_setting_creator_id_fk` (`creator_id`),
  KEY `person_setting_updater_id_fk` (`updater_id`),
  KEY `person_setting_setting_setting_id_fk` (`setting_id`),
  KEY `person_setting_key` (`person_id`,`setting_id`),
  CONSTRAINT `person_setting_creator_id_fk` FOREIGN KEY (`creator_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `person_setting_person_id_fk` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `person_setting_setting_setting_id_fk` FOREIGN KEY (`setting_id`) REFERENCES `setting` (`setting_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `person_setting_updater_id_fk` FOREIGN KEY (`updater_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `result`
--

DROP TABLE IF EXISTS `result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `result` (
  `result_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `node_service_id` int(11) unsigned NOT NULL,
  `zone_id` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `elapsed_time` double NOT NULL,
  `status_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creator_id` int(11) unsigned DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updater_id` int(11) unsigned DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `incident_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`result_id`),
  KEY `result_incident_incident_id_fk` (`incident_id`),
  KEY `result_node_service_zone_index` (`node_service_id`,`zone_id`),
  KEY `result_node_service_zone_start_time_index` (`node_service_id`,`zone_id`,`start_time`),
  KEY `result_node_service_zone_status_code_index` (`node_service_id`,`zone_id`,`status_code`),
  KEY `result_nsz_index` (`node_service_id`,`zone_id`),
  KEY `result_nsz_start_time_index` (`node_service_id`,`zone_id`,`start_time`),
  KEY `result_nsz_status_code_index` (`node_service_id`,`zone_id`,`status_code`),
  KEY `result_nsz_status_time_index` (`node_service_id`,`zone_id`,`status_code`,`start_time`),
  KEY `result_person_creator_id_fk` (`creator_id`),
  KEY `result_person_updater_id_fk` (`updater_id`),
  KEY `result_severity_severity_id_fk` (`status_code`),
  KEY `result_zone_zone_id_fk` (`zone_id`),
  CONSTRAINT `result_incident_incident_id_fk` FOREIGN KEY (`incident_id`) REFERENCES `incident` (`incident_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `result_node_service_node_service_id_fk` FOREIGN KEY (`node_service_id`) REFERENCES `node_service` (`node_service_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `result_person_creator_id_fk` FOREIGN KEY (`creator_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `result_person_updater_id_fk` FOREIGN KEY (`updater_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `result_severity_severity_id_fk` FOREIGN KEY (`status_code`) REFERENCES `severity` (`severity_id`) ON UPDATE CASCADE,
  CONSTRAINT `result_zone_zone_id_fk` FOREIGN KEY (`zone_id`) REFERENCES `zone` (`zone_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `result_latest`
--

DROP TABLE IF EXISTS `result_latest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `result_latest` (
  `result_id` int(10) unsigned DEFAULT NULL,
  `node_service_id` int(11) unsigned NOT NULL,
  `zone_id` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `elapsed_time` double NOT NULL,
  `status_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creator_id` int(11) unsigned DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updater_id` int(11) unsigned DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `incident_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`node_service_id`,`zone_id`),
  KEY `result_latest_incident_incident_id_fk` (`incident_id`),
  KEY `result_latest_node_service_zone_index` (`node_service_id`,`zone_id`),
  KEY `result_latest_node_service_zone_start_time_index` (`node_service_id`,`zone_id`,`start_time`),
  KEY `result_latest_node_service_zone_status_code_index` (`node_service_id`,`zone_id`,`status_code`),
  KEY `result_latest_nsz_index` (`node_service_id`,`zone_id`),
  KEY `result_latest_nsz_start_time_index` (`node_service_id`,`zone_id`,`start_time`),
  KEY `result_latest_nsz_status_code_index` (`node_service_id`,`zone_id`,`status_code`),
  KEY `result_latest_nsz_status_time_index` (`node_service_id`,`zone_id`,`status_code`,`start_time`),
  KEY `result_latest_person_creator_id_fk` (`creator_id`),
  KEY `result_latest_person_updater_id_fk` (`updater_id`),
  KEY `result_latest_severity_severity_id_fk` (`status_code`),
  KEY `result_latest_zone_zone_id_fk` (`zone_id`),
  CONSTRAINT `result_latest_incident_incident_id_fk` FOREIGN KEY (`incident_id`) REFERENCES `incident` (`incident_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `result_latest_node_service_node_service_id_fk` FOREIGN KEY (`node_service_id`) REFERENCES `node_service` (`node_service_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `result_latest_person_creator_id_fk` FOREIGN KEY (`creator_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `result_latest_person_updater_id_fk` FOREIGN KEY (`updater_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `result_latest_severity_severity_id_fk` FOREIGN KEY (`status_code`) REFERENCES `severity` (`severity_id`) ON UPDATE CASCADE,
  CONSTRAINT `result_latest_zone_zone_id_fk` FOREIGN KEY (`zone_id`) REFERENCES `zone` (`zone_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `result_metric`
--

DROP TABLE IF EXISTS `result_metric`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `result_metric` (
  `result_metric_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `result_id` int(11) unsigned NOT NULL,
  `field_name` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metric_type_id` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value_i` int(11) DEFAULT NULL,
  `value_s` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value_f` float DEFAULT NULL,
  `creator_id` int(11) unsigned DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updater_id` int(11) unsigned DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `value_t` mediumtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`result_metric_id`),
  KEY `result_metric_creator_id` (`creator_id`),
  KEY `result_metric_updater_id` (`updater_id`),
  KEY `result_metric_result_id_field_name` (`result_id`,`field_name`),
  KEY `metric_type_creator_id` (`creator_id`),
  KEY `metric_type_result_id_field_name` (`result_id`,`field_name`),
  KEY `metric_type_updater_id` (`updater_id`),
  KEY `result_metric_type_id` (`metric_type_id`),
  CONSTRAINT `result_metric_creator_id` FOREIGN KEY (`creator_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `result_metric_result_id` FOREIGN KEY (`result_id`) REFERENCES `result` (`result_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `result_metric_type_id` FOREIGN KEY (`metric_type_id`) REFERENCES `metric_type` (`metric_type_id`) ON UPDATE CASCADE,
  CONSTRAINT `result_metric_updater_id` FOREIGN KEY (`updater_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service` (
  `service_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `service_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `module_name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `account_id` int(11) unsigned DEFAULT NULL,
  `updater_id` int(11) unsigned DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `creator_id` int(11) unsigned DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`service_id`),
  KEY `service_name` (`service_name`),
  KEY `service_module_name` (`module_name`),
  KEY `service_account_id` (`account_id`),
  KEY `service_updater_id` (`updater_id`),
  KEY `service_creator_id` (`creator_id`),
  CONSTRAINT `service_account_id` FOREIGN KEY (`account_id`) REFERENCES `account` (`account_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `service_creator_id` FOREIGN KEY (`creator_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `service_updater_id` FOREIGN KEY (`updater_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `service_metric_field`
--

DROP TABLE IF EXISTS `service_metric_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_metric_field` (
  `service_metric_field_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `service_id` int(11) unsigned NOT NULL,
  `field_name` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `readable_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `metric_type_id` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `updated_date` datetime DEFAULT NULL,
  `updater_id` int(11) unsigned DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `creator_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`service_metric_field_id`),
  UNIQUE KEY `uniq_smf_service_field_name` (`service_id`,`field_name`),
  KEY `smf_metric_type_id_fk` (`metric_type_id`),
  KEY `smf_updater_id_fk` (`updater_id`),
  KEY `smf_creator_id_fk` (`creator_id`),
  CONSTRAINT `smf_creator_id_fk` FOREIGN KEY (`creator_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `smf_metric_type_id_fk` FOREIGN KEY (`metric_type_id`) REFERENCES `metric_type` (`metric_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `smf_service_id_fk` FOREIGN KEY (`service_id`) REFERENCES `service` (`service_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `smf_updater_id_fk` FOREIGN KEY (`updater_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `setting`
--

DROP TABLE IF EXISTS `setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `setting` (
  `setting_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `setting_key` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `setting_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `setting_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updater_id` int(11) unsigned DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `creator_id` int(11) unsigned DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `setting_default` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`setting_id`),
  UNIQUE KEY `setting_setting_key_uindex` (`setting_key`),
  KEY `setting_person_person_id_fk` (`creator_id`),
  KEY `setting_person_updater_id_fk` (`updater_id`),
  CONSTRAINT `setting_person_person_id_fk` FOREIGN KEY (`creator_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `setting_person_updater_id_fk` FOREIGN KEY (`updater_id`) REFERENCES `person` (`updater_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `severity`
--

DROP TABLE IF EXISTS `severity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `severity` (
  `severity_id` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `severity_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bootstrap_color` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'warning',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `updater_id` int(11) unsigned DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `creator_id` int(11) unsigned DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `layout_color` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'aqua',
  `layout_icon` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'information-circled',
  `layout_icon_vendor` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ion',
  PRIMARY KEY (`severity_id`),
  UNIQUE KEY `severity_severity_id_uindex` (`severity_id`),
  KEY `sv_creator_id` (`creator_id`),
  KEY `sv_updater_id` (`updater_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zone`
--

DROP TABLE IF EXISTS `zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zone` (
  `zone_id` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zone_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sort_order` int(10) NOT NULL DEFAULT '0',
  `account_id` int(11) unsigned DEFAULT NULL,
  `updater_id` int(11) unsigned DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `creator_id` int(11) unsigned DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`zone_id`),
  KEY `zone_name` (`zone_name`),
  KEY `zone_account_id` (`account_id`),
  KEY `zone_updater_id` (`updater_id`),
  KEY `zone_creator_id` (`creator_id`),
  CONSTRAINT `zone_account_id` FOREIGN KEY (`account_id`) REFERENCES `account` (`account_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `zone_creator_id` FOREIGN KEY (`creator_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `zone_updater_id` FOREIGN KEY (`updater_id`) REFERENCES `person` (`person_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zz_audit_account`
--

DROP TABLE IF EXISTS `zz_audit_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_audit_account` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` char(1) NOT NULL DEFAULT 'I' COMMENT 'I - Insert, U - Update, D - Delete',
  `old_id` int(11) unsigned DEFAULT NULL,
  `new_id` int(11) unsigned DEFAULT NULL,
  `event_date` datetime DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zz_audit_account_notifier`
--

DROP TABLE IF EXISTS `zz_audit_account_notifier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_audit_account_notifier` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` char(1) NOT NULL DEFAULT 'I' COMMENT 'I - Insert, U - Update, D - Delete',
  `old_id` int(11) unsigned DEFAULT NULL,
  `new_id` int(11) unsigned DEFAULT NULL,
  `event_date` datetime DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zz_audit_account_person`
--

DROP TABLE IF EXISTS `zz_audit_account_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_audit_account_person` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` char(1) NOT NULL DEFAULT 'I' COMMENT 'I - Insert, U - Update, D - Delete',
  `old_id` int(11) unsigned DEFAULT NULL,
  `new_id` int(11) unsigned DEFAULT NULL,
  `event_date` datetime DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zz_audit_environment`
--

DROP TABLE IF EXISTS `zz_audit_environment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_audit_environment` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` char(1) NOT NULL DEFAULT 'I' COMMENT 'I - Insert, U - Update, D - Delete',
  `old_id` int(11) unsigned DEFAULT NULL,
  `new_id` int(11) unsigned DEFAULT NULL,
  `event_date` datetime DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zz_audit_incident`
--

DROP TABLE IF EXISTS `zz_audit_incident`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_audit_incident` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` char(1) NOT NULL DEFAULT 'I' COMMENT 'I - Insert, U - Update, D - Delete',
  `old_id` int(11) unsigned DEFAULT NULL,
  `new_id` int(11) unsigned DEFAULT NULL,
  `event_date` datetime DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zz_audit_incident_notifier_history`
--

DROP TABLE IF EXISTS `zz_audit_incident_notifier_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_audit_incident_notifier_history` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` char(1) NOT NULL DEFAULT 'I' COMMENT 'I - Insert, U - Update, D - Delete',
  `old_id` int(11) unsigned DEFAULT NULL,
  `new_id` int(11) unsigned DEFAULT NULL,
  `event_date` datetime DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zz_audit_metric_type`
--

DROP TABLE IF EXISTS `zz_audit_metric_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_audit_metric_type` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` char(1) NOT NULL DEFAULT 'I' COMMENT 'I - Insert, U - Update, D - Delete',
  `old_id` varchar(5) DEFAULT NULL,
  `new_id` varchar(5) DEFAULT NULL,
  `event_date` datetime DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zz_audit_node`
--

DROP TABLE IF EXISTS `zz_audit_node`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_audit_node` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` char(1) NOT NULL DEFAULT 'I' COMMENT 'I - Insert, U - Update, D - Delete',
  `old_id` int(11) unsigned DEFAULT NULL,
  `new_id` int(11) unsigned DEFAULT NULL,
  `event_date` datetime DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zz_audit_node_service`
--

DROP TABLE IF EXISTS `zz_audit_node_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_audit_node_service` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` char(1) NOT NULL DEFAULT 'I' COMMENT 'I - Insert, U - Update, D - Delete',
  `old_id` int(11) unsigned DEFAULT NULL,
  `new_id` int(11) unsigned DEFAULT NULL,
  `event_date` datetime DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zz_audit_node_service_notifier`
--

DROP TABLE IF EXISTS `zz_audit_node_service_notifier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_audit_node_service_notifier` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` char(1) NOT NULL DEFAULT 'I' COMMENT 'I - Insert, U - Update, D - Delete',
  `old_id` int(11) unsigned DEFAULT NULL,
  `new_id` int(11) unsigned DEFAULT NULL,
  `event_date` datetime DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zz_audit_node_service_zone`
--

DROP TABLE IF EXISTS `zz_audit_node_service_zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_audit_node_service_zone` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` char(1) NOT NULL DEFAULT 'I' COMMENT 'I - Insert, U - Update, D - Delete',
  `old_id` int(11) unsigned DEFAULT NULL,
  `new_id` int(11) unsigned DEFAULT NULL,
  `event_date` datetime DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zz_audit_notifier`
--

DROP TABLE IF EXISTS `zz_audit_notifier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_audit_notifier` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` char(1) NOT NULL DEFAULT 'I' COMMENT 'I - Insert, U - Update, D - Delete',
  `old_id` int(11) unsigned DEFAULT NULL,
  `new_id` int(11) unsigned DEFAULT NULL,
  `event_date` datetime DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zz_audit_person`
--

DROP TABLE IF EXISTS `zz_audit_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_audit_person` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` char(1) NOT NULL DEFAULT 'I' COMMENT 'I - Insert, U - Update, D - Delete',
  `old_id` int(11) unsigned DEFAULT NULL,
  `new_id` int(11) unsigned DEFAULT NULL,
  `event_date` datetime DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zz_audit_person_setting`
--

DROP TABLE IF EXISTS `zz_audit_person_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_audit_person_setting` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` char(1) NOT NULL DEFAULT 'I' COMMENT 'I - Insert, U - Update, D - Delete',
  `old_id` int(11) DEFAULT NULL,
  `new_id` int(11) DEFAULT NULL,
  `event_date` datetime DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zz_audit_result`
--

DROP TABLE IF EXISTS `zz_audit_result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_audit_result` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` char(1) NOT NULL DEFAULT 'I' COMMENT 'I - Insert, U - Update, D - Delete',
  `old_id` int(10) unsigned DEFAULT NULL,
  `new_id` int(10) unsigned DEFAULT NULL,
  `event_date` datetime DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zz_audit_result_metric`
--

DROP TABLE IF EXISTS `zz_audit_result_metric`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_audit_result_metric` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` char(1) NOT NULL DEFAULT 'I' COMMENT 'I - Insert, U - Update, D - Delete',
  `old_id` int(11) unsigned DEFAULT NULL,
  `new_id` int(11) unsigned DEFAULT NULL,
  `event_date` datetime DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zz_audit_service`
--

DROP TABLE IF EXISTS `zz_audit_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_audit_service` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` char(1) NOT NULL DEFAULT 'I' COMMENT 'I - Insert, U - Update, D - Delete',
  `old_id` int(11) unsigned DEFAULT NULL,
  `new_id` int(11) unsigned DEFAULT NULL,
  `event_date` datetime DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zz_audit_service_metric_field`
--

DROP TABLE IF EXISTS `zz_audit_service_metric_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_audit_service_metric_field` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` char(1) NOT NULL DEFAULT 'I' COMMENT 'I - Insert, U - Update, D - Delete',
  `old_id` int(11) unsigned DEFAULT NULL,
  `new_id` int(11) unsigned DEFAULT NULL,
  `event_date` datetime DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zz_audit_setting`
--

DROP TABLE IF EXISTS `zz_audit_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_audit_setting` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` char(1) NOT NULL DEFAULT 'I' COMMENT 'I - Insert, U - Update, D - Delete',
  `old_id` int(11) unsigned DEFAULT NULL,
  `new_id` int(11) unsigned DEFAULT NULL,
  `event_date` datetime DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zz_audit_severity`
--

DROP TABLE IF EXISTS `zz_audit_severity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_audit_severity` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` char(1) NOT NULL DEFAULT 'I' COMMENT 'I - Insert, U - Update, D - Delete',
  `old_id` varchar(10) DEFAULT NULL,
  `new_id` varchar(10) DEFAULT NULL,
  `event_date` datetime DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zz_audit_zone`
--

DROP TABLE IF EXISTS `zz_audit_zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_audit_zone` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` char(1) NOT NULL DEFAULT 'I' COMMENT 'I - Insert, U - Update, D - Delete',
  `old_id` varchar(10) DEFAULT NULL,
  `new_id` varchar(10) DEFAULT NULL,
  `event_date` datetime DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zz_subscription`
--

DROP TABLE IF EXISTS `zz_subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_subscription` (
  `subscription_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `db_table_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `db_event_type` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'I' COMMENT 'I - Insert, U - Update, D - Delete',
  PRIMARY KEY (`subscription_id`),
  UNIQUE KEY `uniq_event` (`client_id`,`db_event_type`,`db_table_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zz_subscription_event`
--

DROP TABLE IF EXISTS `zz_subscription_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_subscription_event` (
  `event_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `subscription_id` int(11) unsigned NOT NULL,
  `client_id` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `db_table_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `db_event_type` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'I' COMMENT 'I - Insert, U - Update, D - Delete',
  `zz_audit_id` int(11) unsigned NOT NULL,
  `event_date` datetime NOT NULL,
  PRIMARY KEY (`event_id`),
  KEY `zz_sub_event_client_type` (`client_id`,`db_event_type`),
  KEY `zz_subscription_event_zz_subscription_subscription_id_fk` (`subscription_id`),
  CONSTRAINT `zz_subscription_event_zz_subscription_subscription_id_fk` FOREIGN KEY (`subscription_id`) REFERENCES `zz_subscription` (`subscription_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-07 22:04:33
