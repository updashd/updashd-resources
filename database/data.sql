-- MySQL dump 10.13  Distrib 5.7.16, for Win64 (x86_64)
--
-- Host: localhost    Database: updashdtwo
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `severity`
--

LOCK TABLES `severity` WRITE;
/*!40000 ALTER TABLE `severity` DISABLE KEYS */;
INSERT INTO `severity` VALUES ('CONF','Config','warning',1,NULL,NULL,NULL,NULL,'purple','wrench','ion'),('CRITICAL','Critical','danger',5,NULL,NULL,NULL,NULL,'red','alert-circled','ion'),('ERR','Error','danger',6,NULL,NULL,NULL,NULL,'red','alert-circled','ion'),('NOTICE','Notice','info',3,NULL,NULL,NULL,NULL,'aqua','information-circled','ion'),('SUCCESS','Successful','success',2,NULL,NULL,NULL,NULL,'green','checkmark-circled','ion'),('WARNING','Warning','warning',4,NULL,NULL,NULL,NULL,'orange','alert','ion');
/*!40000 ALTER TABLE `severity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` VALUES (13,'Curl HTTP','curl_http',1,NULL,NULL,'2017-12-08 04:37:40',NULL,'2017-12-08 04:37:40'),(14,'Curl HTTPS','curl_https',2,NULL,NULL,'2017-12-08 04:37:40',NULL,'2017-12-08 04:37:40'),(15,'ICMP Ping','icmp_ping',3,NULL,NULL,'2017-12-08 04:37:40',NULL,'2017-12-08 04:37:40'),(16,'DNS','dns',4,NULL,NULL,'2017-12-08 04:37:40',NULL,'2017-12-08 04:37:40'),(17,'MySQL Replication','mysql_replication',5,NULL,NULL,'2017-12-08 04:37:40',NULL,'2017-12-08 04:37:40'),(18,'MySQL Ping','mysql_ping',6,NULL,NULL,'2017-12-08 04:37:40',NULL,'2017-12-08 04:37:40');
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `notifier`
--

LOCK TABLES `notifier` WRITE;
/*!40000 ALTER TABLE `notifier` DISABLE KEYS */;
INSERT INTO `notifier` VALUES (2,'Slack','slack',1,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `notifier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `setting`
--

LOCK TABLES `setting` WRITE;
/*!40000 ALTER TABLE `setting` DISABLE KEYS */;
INSERT INTO `setting` VALUES (1,'timezone','Time Zone','This is the user\'s timezone.',NULL,NULL,NULL,NULL,'UTC'),(2,'date_format','Date Format','How to display a date (when there is no time)',NULL,NULL,NULL,NULL,'Y-m-d'),(3,'time_format','Time Format','How to display a time (when there is no date)',NULL,NULL,NULL,NULL,'H:i:s T O'),(4,'datetime_format','Date and Time Format','How to display both a date and time.',NULL,NULL,NULL,NULL,'Y-m-d H:i:s T');
/*!40000 ALTER TABLE `setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `metric_type`
--

LOCK TABLES `metric_type` WRITE;
/*!40000 ALTER TABLE `metric_type` DISABLE KEYS */;
INSERT INTO `metric_type` VALUES ('FLOAT','Floating Point',NULL,NULL,NULL,NULL),('INT','Integer',NULL,NULL,NULL,NULL),('STR','String (0-100 char)',NULL,NULL,NULL,NULL),('TXT','Text',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `metric_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-07 22:04:33
