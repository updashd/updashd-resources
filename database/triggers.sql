-- MySQL dump 10.13  Distrib 5.7.16, for Win64 (x86_64)
--
-- Host: localhost    Database: updashdtwo
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER after_account_insert AFTER INSERT
ON account
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_account (event_type, old_id, new_id, event_date) VALUES ('I', NULL, NEW.account_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'account' AND db_event_type = 'I'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_account_update BEFORE UPDATE
ON account
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_account (event_type, old_id, new_id, event_date) VALUES ('U', OLD.account_id, NEW.account_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'account' AND db_event_type = 'U'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_account_delete BEFORE DELETE
ON account
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_account (event_type, old_id, new_id, event_date) VALUES ('D', OLD.account_id, NULL, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'account' AND db_event_type = 'D'
;
    DELETE FROM account_person WHERE account_id = OLD.account_id;
    DELETE FROM account_notifier WHERE account_id = OLD.account_id;
    DELETE FROM environment WHERE account_id = OLD.account_id;
    DELETE FROM incident WHERE account_id = OLD.account_id;
    DELETE FROM node WHERE account_id = OLD.account_id;
    DELETE FROM notifier WHERE account_id = OLD.account_id;
    DELETE FROM service WHERE account_id = OLD.account_id;
    DELETE FROM zone WHERE account_id = OLD.account_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER after_account_notifier_insert AFTER INSERT
ON account_notifier
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_account_notifier (event_type, old_id, new_id, event_date) VALUES ('I', NULL, NEW.account_notifier_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'account_notifier' AND db_event_type = 'I'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_account_notifier_update BEFORE UPDATE
ON account_notifier
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_account_notifier (event_type, old_id, new_id, event_date) VALUES ('U', OLD.account_notifier_id, NEW.account_notifier_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'account_notifier' AND db_event_type = 'U'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_account_notifier_delete BEFORE DELETE
ON account_notifier
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_account_notifier (event_type, old_id, new_id, event_date) VALUES ('D', OLD.account_notifier_id, NULL, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'account_notifier' AND db_event_type = 'D'
;
    DELETE FROM incident_notifier_history WHERE account_notifier_id = OLD.account_notifier_id;
    DELETE FROM node_service_notifier WHERE account_notifier_id = OLD.account_notifier_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER after_account_person_insert AFTER INSERT
ON account_person
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_account_person (event_type, old_id, new_id, event_date) VALUES ('I', NULL, NEW.account_person_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'account_person' AND db_event_type = 'I'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_account_person_update BEFORE UPDATE
ON account_person
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_account_person (event_type, old_id, new_id, event_date) VALUES ('U', OLD.account_person_id, NEW.account_person_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'account_person' AND db_event_type = 'U'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_account_person_delete BEFORE DELETE
ON account_person
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_account_person (event_type, old_id, new_id, event_date) VALUES ('D', OLD.account_person_id, NULL, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'account_person' AND db_event_type = 'D'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER after_environment_insert AFTER INSERT
ON environment
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_environment (event_type, old_id, new_id, event_date) VALUES ('I', NULL, NEW.environment_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'environment' AND db_event_type = 'I'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_environment_update BEFORE UPDATE
ON environment
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_environment (event_type, old_id, new_id, event_date) VALUES ('U', OLD.environment_id, NEW.environment_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'environment' AND db_event_type = 'U'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_environment_delete BEFORE DELETE
ON environment
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_environment (event_type, old_id, new_id, event_date) VALUES ('D', OLD.environment_id, NULL, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'environment' AND db_event_type = 'D'
;
    DELETE FROM node WHERE environment_id = OLD.environment_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER after_incident_insert AFTER INSERT
ON incident
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_incident (event_type, old_id, new_id, event_date) VALUES ('I', NULL, NEW.incident_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'incident' AND db_event_type = 'I'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_incident_update BEFORE UPDATE
ON incident
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_incident (event_type, old_id, new_id, event_date) VALUES ('U', OLD.incident_id, NEW.incident_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'incident' AND db_event_type = 'U'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_incident_delete BEFORE DELETE
ON incident
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_incident (event_type, old_id, new_id, event_date) VALUES ('D', OLD.incident_id, NULL, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'incident' AND db_event_type = 'D'
;
    DELETE FROM incident_notifier_history WHERE incident_id = OLD.incident_id;
    UPDATE  result SET incident_id = NULL WHERE incident_id = OLD.incident_id;
    UPDATE  result_latest SET incident_id = NULL WHERE incident_id = OLD.incident_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER after_incident_notifier_history_insert AFTER INSERT
ON incident_notifier_history
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_incident_notifier_history (event_type, old_id, new_id, event_date) VALUES ('I', NULL, NEW.incident_notifier_history_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'incident_notifier_history' AND db_event_type = 'I'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_incident_notifier_history_update BEFORE UPDATE
ON incident_notifier_history
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_incident_notifier_history (event_type, old_id, new_id, event_date) VALUES ('U', OLD.incident_notifier_history_id, NEW.incident_notifier_history_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'incident_notifier_history' AND db_event_type = 'U'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_incident_notifier_history_delete BEFORE DELETE
ON incident_notifier_history
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_incident_notifier_history (event_type, old_id, new_id, event_date) VALUES ('D', OLD.incident_notifier_history_id, NULL, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'incident_notifier_history' AND db_event_type = 'D'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER after_metric_type_insert AFTER INSERT
ON metric_type
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_metric_type (event_type, old_id, new_id, event_date) VALUES ('I', NULL, NEW.metric_type_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'metric_type' AND db_event_type = 'I'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_metric_type_update BEFORE UPDATE
ON metric_type
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_metric_type (event_type, old_id, new_id, event_date) VALUES ('U', OLD.metric_type_id, NEW.metric_type_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'metric_type' AND db_event_type = 'U'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_metric_type_delete BEFORE DELETE
ON metric_type
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_metric_type (event_type, old_id, new_id, event_date) VALUES ('D', OLD.metric_type_id, NULL, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'metric_type' AND db_event_type = 'D'
;
    DELETE FROM service_metric_field WHERE metric_type_id = OLD.metric_type_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER after_node_insert AFTER INSERT
ON node
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_node (event_type, old_id, new_id, event_date) VALUES ('I', NULL, NEW.node_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'node' AND db_event_type = 'I'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_node_update BEFORE UPDATE
ON node
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_node (event_type, old_id, new_id, event_date) VALUES ('U', OLD.node_id, NEW.node_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'node' AND db_event_type = 'U'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_node_delete BEFORE DELETE
ON node
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_node (event_type, old_id, new_id, event_date) VALUES ('D', OLD.node_id, NULL, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'node' AND db_event_type = 'D'
;
    DELETE FROM node_service WHERE node_id = OLD.node_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER after_node_service_insert AFTER INSERT
ON node_service
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_node_service (event_type, old_id, new_id, event_date) VALUES ('I', NULL, NEW.node_service_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'node_service' AND db_event_type = 'I'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_node_service_update BEFORE UPDATE
ON node_service
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_node_service (event_type, old_id, new_id, event_date) VALUES ('U', OLD.node_service_id, NEW.node_service_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'node_service' AND db_event_type = 'U'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_node_service_delete BEFORE DELETE
ON node_service
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_node_service (event_type, old_id, new_id, event_date) VALUES ('D', OLD.node_service_id, NULL, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'node_service' AND db_event_type = 'D'
;
    DELETE FROM incident WHERE node_service_id = OLD.node_service_id;
    DELETE FROM node_service_notifier WHERE node_service_id = OLD.node_service_id;
    DELETE FROM node_service_zone WHERE node_service_id = OLD.node_service_id;
    DELETE FROM result_latest WHERE node_service_id = OLD.node_service_id;
    DELETE FROM result WHERE node_service_id = OLD.node_service_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER after_node_service_notifier_insert AFTER INSERT
ON node_service_notifier
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_node_service_notifier (event_type, old_id, new_id, event_date) VALUES ('I', NULL, NEW.node_service_notifier_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'node_service_notifier' AND db_event_type = 'I'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_node_service_notifier_update BEFORE UPDATE
ON node_service_notifier
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_node_service_notifier (event_type, old_id, new_id, event_date) VALUES ('U', OLD.node_service_notifier_id, NEW.node_service_notifier_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'node_service_notifier' AND db_event_type = 'U'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_node_service_notifier_delete BEFORE DELETE
ON node_service_notifier
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_node_service_notifier (event_type, old_id, new_id, event_date) VALUES ('D', OLD.node_service_notifier_id, NULL, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'node_service_notifier' AND db_event_type = 'D'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER after_node_service_zone_insert AFTER INSERT
ON node_service_zone
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_node_service_zone (event_type, old_id, new_id, event_date) VALUES ('I', NULL, NEW.node_service_zone_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'node_service_zone' AND db_event_type = 'I'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_node_service_zone_update BEFORE UPDATE
ON node_service_zone
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_node_service_zone (event_type, old_id, new_id, event_date) VALUES ('U', OLD.node_service_zone_id, NEW.node_service_zone_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'node_service_zone' AND db_event_type = 'U'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_node_service_zone_delete BEFORE DELETE
ON node_service_zone
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_node_service_zone (event_type, old_id, new_id, event_date) VALUES ('D', OLD.node_service_zone_id, NULL, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'node_service_zone' AND db_event_type = 'D'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER after_notifier_insert AFTER INSERT
ON notifier
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_notifier (event_type, old_id, new_id, event_date) VALUES ('I', NULL, NEW.notifier_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'notifier' AND db_event_type = 'I'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_notifier_update BEFORE UPDATE
ON notifier
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_notifier (event_type, old_id, new_id, event_date) VALUES ('U', OLD.notifier_id, NEW.notifier_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'notifier' AND db_event_type = 'U'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_notifier_delete BEFORE DELETE
ON notifier
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_notifier (event_type, old_id, new_id, event_date) VALUES ('D', OLD.notifier_id, NULL, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'notifier' AND db_event_type = 'D'
;
    DELETE FROM account_notifier WHERE notifier_id = OLD.notifier_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER after_person_insert AFTER INSERT
ON person
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_person (event_type, old_id, new_id, event_date) VALUES ('I', NULL, NEW.person_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'person' AND db_event_type = 'I'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_person_update BEFORE UPDATE
ON person
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_person (event_type, old_id, new_id, event_date) VALUES ('U', OLD.person_id, NEW.person_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'person' AND db_event_type = 'U'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_person_delete BEFORE DELETE
ON person
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_person (event_type, old_id, new_id, event_date) VALUES ('D', OLD.person_id, NULL, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'person' AND db_event_type = 'D'
;
    UPDATE  account SET creator_id = NULL WHERE creator_id = OLD.person_id;
    UPDATE  account SET owner_id = NULL WHERE owner_id = OLD.person_id;
    UPDATE  account_person SET creator_id = NULL WHERE creator_id = OLD.person_id;
    DELETE FROM account_person WHERE person_id = OLD.person_id;
    UPDATE  account_person SET updater_id = NULL WHERE updater_id = OLD.person_id;
    UPDATE  account SET updater_id = NULL WHERE updater_id = OLD.person_id;
    UPDATE  account_notifier SET creator_id = NULL WHERE creator_id = OLD.person_id;
    UPDATE  account_notifier SET updater_id = NULL WHERE updater_id = OLD.person_id;
    UPDATE  environment SET creator_id = NULL WHERE creator_id = OLD.person_id;
    UPDATE  environment SET updater_id = NULL WHERE updater_id = OLD.person_id;
    UPDATE  incident SET creator_id = NULL WHERE creator_id = OLD.person_id;
    UPDATE  incident SET updater_id = NULL WHERE updater_id = OLD.person_id;
    UPDATE  incident_notifier_history SET creator_id = NULL WHERE creator_id = OLD.person_id;
    UPDATE  incident_notifier_history SET updater_id = NULL WHERE updater_id = OLD.person_id;
    UPDATE  metric_type SET creator_id = NULL WHERE creator_id = OLD.person_id;
    UPDATE  metric_type SET updater_id = NULL WHERE updater_id = OLD.person_id;
    UPDATE  node SET creator_id = NULL WHERE creator_id = OLD.person_id;
    UPDATE  node SET updater_id = NULL WHERE updater_id = OLD.person_id;
    UPDATE  notifier SET creator_id = NULL WHERE creator_id = OLD.person_id;
    UPDATE  notifier SET updater_id = NULL WHERE updater_id = OLD.person_id;
    UPDATE  node_service_notifier SET creator_id = NULL WHERE creator_id = OLD.person_id;
    UPDATE  node_service_notifier SET updater_id = NULL WHERE updater_id = OLD.person_id;
    UPDATE  node_service_zone SET creator_id = NULL WHERE creator_id = OLD.person_id;
    UPDATE  node_service_zone SET updater_id = NULL WHERE updater_id = OLD.person_id;
    UPDATE  node_service SET creator_id = NULL WHERE creator_id = OLD.person_id;
    UPDATE  node_service SET updater_id = NULL WHERE updater_id = OLD.person_id;
    UPDATE  person_setting SET creator_id = NULL WHERE creator_id = OLD.person_id;
    DELETE FROM person_setting WHERE person_id = OLD.person_id;
    UPDATE  person_setting SET updater_id = NULL WHERE updater_id = OLD.person_id;
    UPDATE  result_latest SET creator_id = NULL WHERE creator_id = OLD.person_id;
    UPDATE  result_latest SET updater_id = NULL WHERE updater_id = OLD.person_id;
    UPDATE  result_metric SET creator_id = NULL WHERE creator_id = OLD.person_id;
    UPDATE  result_metric SET updater_id = NULL WHERE updater_id = OLD.person_id;
    UPDATE  result SET creator_id = NULL WHERE creator_id = OLD.person_id;
    UPDATE  result SET updater_id = NULL WHERE updater_id = OLD.person_id;
    UPDATE  service SET creator_id = NULL WHERE creator_id = OLD.person_id;
    UPDATE  service SET updater_id = NULL WHERE updater_id = OLD.person_id;
    UPDATE  setting SET creator_id = NULL WHERE creator_id = OLD.person_id;
    UPDATE  setting SET updater_id = NULL WHERE updater_id = OLD.updater_id;
    UPDATE  service_metric_field SET creator_id = NULL WHERE creator_id = OLD.person_id;
    UPDATE  service_metric_field SET updater_id = NULL WHERE updater_id = OLD.person_id;
    UPDATE  zone SET creator_id = NULL WHERE creator_id = OLD.person_id;
    UPDATE  zone SET updater_id = NULL WHERE updater_id = OLD.person_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER after_person_setting_insert AFTER INSERT
ON person_setting
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_person_setting (event_type, old_id, new_id, event_date) VALUES ('I', NULL, NEW.person_setting_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'person_setting' AND db_event_type = 'I'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_person_setting_update BEFORE UPDATE
ON person_setting
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_person_setting (event_type, old_id, new_id, event_date) VALUES ('U', OLD.person_setting_id, NEW.person_setting_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'person_setting' AND db_event_type = 'U'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_person_setting_delete BEFORE DELETE
ON person_setting
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_person_setting (event_type, old_id, new_id, event_date) VALUES ('D', OLD.person_setting_id, NULL, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'person_setting' AND db_event_type = 'D'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER after_result_insert AFTER INSERT
ON result
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_result (event_type, old_id, new_id, event_date) VALUES ('I', NULL, NEW.result_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'result' AND db_event_type = 'I'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_result_update BEFORE UPDATE
ON result
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_result (event_type, old_id, new_id, event_date) VALUES ('U', OLD.result_id, NEW.result_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'result' AND db_event_type = 'U'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_result_delete BEFORE DELETE
ON result
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_result (event_type, old_id, new_id, event_date) VALUES ('D', OLD.result_id, NULL, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'result' AND db_event_type = 'D'
;
    DELETE FROM result_metric WHERE result_id = OLD.result_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER after_result_metric_insert AFTER INSERT
ON result_metric
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_result_metric (event_type, old_id, new_id, event_date) VALUES ('I', NULL, NEW.result_metric_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'result_metric' AND db_event_type = 'I'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_result_metric_update BEFORE UPDATE
ON result_metric
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_result_metric (event_type, old_id, new_id, event_date) VALUES ('U', OLD.result_metric_id, NEW.result_metric_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'result_metric' AND db_event_type = 'U'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_result_metric_delete BEFORE DELETE
ON result_metric
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_result_metric (event_type, old_id, new_id, event_date) VALUES ('D', OLD.result_metric_id, NULL, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'result_metric' AND db_event_type = 'D'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER after_service_insert AFTER INSERT
ON service
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_service (event_type, old_id, new_id, event_date) VALUES ('I', NULL, NEW.service_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'service' AND db_event_type = 'I'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_service_update BEFORE UPDATE
ON service
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_service (event_type, old_id, new_id, event_date) VALUES ('U', OLD.service_id, NEW.service_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'service' AND db_event_type = 'U'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_service_delete BEFORE DELETE
ON service
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_service (event_type, old_id, new_id, event_date) VALUES ('D', OLD.service_id, NULL, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'service' AND db_event_type = 'D'
;
    DELETE FROM node_service WHERE service_id = OLD.service_id;
    DELETE FROM service_metric_field WHERE service_id = OLD.service_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER after_service_metric_field_insert AFTER INSERT
ON service_metric_field
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_service_metric_field (event_type, old_id, new_id, event_date) VALUES ('I', NULL, NEW.service_metric_field_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'service_metric_field' AND db_event_type = 'I'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_service_metric_field_update BEFORE UPDATE
ON service_metric_field
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_service_metric_field (event_type, old_id, new_id, event_date) VALUES ('U', OLD.service_metric_field_id, NEW.service_metric_field_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'service_metric_field' AND db_event_type = 'U'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_service_metric_field_delete BEFORE DELETE
ON service_metric_field
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_service_metric_field (event_type, old_id, new_id, event_date) VALUES ('D', OLD.service_metric_field_id, NULL, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'service_metric_field' AND db_event_type = 'D'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER after_setting_insert AFTER INSERT
ON setting
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_setting (event_type, old_id, new_id, event_date) VALUES ('I', NULL, NEW.setting_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'setting' AND db_event_type = 'I'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_setting_update BEFORE UPDATE
ON setting
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_setting (event_type, old_id, new_id, event_date) VALUES ('U', OLD.setting_id, NEW.setting_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'setting' AND db_event_type = 'U'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_setting_delete BEFORE DELETE
ON setting
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_setting (event_type, old_id, new_id, event_date) VALUES ('D', OLD.setting_id, NULL, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'setting' AND db_event_type = 'D'
;
    DELETE FROM person_setting WHERE setting_id = OLD.setting_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER after_severity_insert AFTER INSERT
ON severity
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_severity (event_type, old_id, new_id, event_date) VALUES ('I', NULL, NEW.severity_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'severity' AND db_event_type = 'I'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_severity_update BEFORE UPDATE
ON severity
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_severity (event_type, old_id, new_id, event_date) VALUES ('U', OLD.severity_id, NEW.severity_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'severity' AND db_event_type = 'U'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_severity_delete BEFORE DELETE
ON severity
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_severity (event_type, old_id, new_id, event_date) VALUES ('D', OLD.severity_id, NULL, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'severity' AND db_event_type = 'D'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER after_zone_insert AFTER INSERT
ON zone
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_zone (event_type, old_id, new_id, event_date) VALUES ('I', NULL, NEW.zone_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'zone' AND db_event_type = 'I'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_zone_update BEFORE UPDATE
ON zone
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_zone (event_type, old_id, new_id, event_date) VALUES ('U', OLD.zone_id, NEW.zone_id, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'zone' AND db_event_type = 'U'
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER before_zone_delete BEFORE DELETE
ON zone
FOR EACH ROW
BEGIN
    INSERT INTO zz_audit_zone (event_type, old_id, new_id, event_date) VALUES ('D', OLD.zone_id, NULL, NOW());
    
    INSERT INTO zz_subscription_event (subscription_id, client_id, db_event_type, db_table_name, zz_audit_id, event_date)
    SELECT subscription_id, client_id, db_event_type, db_table_name, LAST_INSERT_ID(), NOW()
    FROM zz_subscription
    WHERE db_table_name = 'zone' AND db_event_type = 'D'
;
    DELETE FROM node_service_zone WHERE zone_id = OLD.zone_id;
    DELETE FROM result_latest WHERE zone_id = OLD.zone_id;
    DELETE FROM result WHERE zone_id = OLD.zone_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-07 22:04:33
