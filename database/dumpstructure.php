<?php
require 'src/Parameter.php';
require 'src/Command.php';
$config = require 'config.php';

$structureFile = 'structure.sql';
$triggersFile = 'triggers.sql';
$dataFile = 'data.sql';

$dump = new Command('mysqldump');
$dump->addParameterFlagValue('-u', $config['username']);
$dump->addParameterFlagValue('-p', $config['password'], '');
$dump->addParameterFlagValue('-h', $config['host']);
$dump->addParameterFlagValue('-P', $config['port']);

$structureDumper = clone $dump;
$structureDumper->addParameterFlag('--skip-triggers');
$structureDumper->addParameterFlag('-d');
$structureDumper->addParameterString($config['dbname']);
$structureDumper->setPipeToFile($structureFile);
$structureDumper->execute(true);

file_put_contents($structureFile, preg_replace('/ AUTO_INCREMENT=[0-9]+/', '', file_get_contents($structureFile)));

$triggerDumper = clone $dump;
$triggerDumper->addParameterFlag('--no-create-info');
$triggerDumper->addParameterFlag('-d');
$triggerDumper->addParameterString($config['dbname']);
$triggerDumper->setPipeToFile($triggersFile);
$triggerDumper->execute(true);

$dataDumper = clone $dump;
$dataDumper->addParameterFlag('--no-create-info');
$dataDumper->addParameterFlag('--skip-triggers');
$dataDumper->addParameterString($config['dbname']);
$dataDumper->addParameterString('severity');
$dataDumper->addParameterString('service');
$dataDumper->addParameterString('notifier');
$dataDumper->addParameterString('setting');
$dataDumper->addParameterString('metric_type');
$dataDumper->setPipeToFile($dataFile);
$dataDumper->passthru(true);
