<?php

$outputDir = __DIR__ . '/config';

$installDir = __DIR__ . '/config/test';

$configVars = [
    // Redis
    'redisHost' => 'updashd-redis',
    'redisPort' => 6379,

    // MySQL
    'mysqlHost' => 'updashd-mysql',
    'mysqlPort' => 3306,
    'mysqlDataBase' => 'updashd',
    'mysqlUserName' => 'updashd',
    'mysqlPassword' => 'mypassword123',

    // Zones
    'zone' => 'default',

    // Keys
    'notifierPrivateKey' => $outputDir . '/worker_key.pem',
    'notifierPublicKey' => $outputDir . '/worker_pub.pem',
    'workerPrivateKey' => $outputDir . '/notifier_key.pem',
    'workerPublicKey' => $outputDir . '/notifier_pub.pem',

    // Debug
    'debug' => 'false',
    'caching' => 'false'
];

$files = [
    // Name => Install Location
    'agent.php' => 'updashd-agent/config/config.local.php',

    'dispatcher.php' => 'updashd-dispatcher/config/config.local.php',

    'notifier.php' => 'updashd-notifier-php/config/config.local.php',

    'recorder.php' => 'updashd-recorder/config/config.local.php',

    'rescheduler.php' => 'updashd-rescheduler/config/config.local.php',

    'worker.php' => 'updashd-worker-php/config/config.local.php',

    'webapp_application.php' => 'updashd-webapp/config/application.local.php',
    'webapp_cache.php' => 'updashd-webapp/config/autoload/cache.local.php',
    'webapp_doctrine.php' => 'updashd-webapp/config/autoload/doctrine.local.php',
    'webapp_notifier.php' => 'updashd-webapp/config/autoload/notifier.local.php',
    'webapp_predis.php' => 'updashd-webapp/config/autoload/predis.local.php',
    'webapp_worker.php' => 'updashd-webapp/config/autoload/worker.local.php',
];

class View {
    protected $vars;

    public function __construct ($vars) {
        $this->vars = $vars;
    }

    public function __get ($name) {
        return array_key_exists($name, $this->vars) ? $this->vars[$name] : null;
    }

    public function render ($file) {
        ob_start();
        include $file;
        $content = ob_get_contents();
        ob_clean();
        return $content;
    }
}

@mkdir($outputDir, 0466, true);

$view = new View($configVars);

foreach ($files as $sourceFile => $installLocation) {
    $templateFileName = __DIR__ . '/configtemplates/' . $sourceFile;
    $outputFileName = $outputDir . '/' . basename($templateFileName);
    $installLocation = $installDir . '/' . $installLocation;

    echo 'Generating: ' . $templateFileName . PHP_EOL;

    $output = $view->render($templateFileName);

    file_put_contents($outputFileName, $output);

    $outputFileName = realpath($outputFileName);

    if (! file_exists($installLocation) || is_link($installLocation)) {

        if (is_link($installLocation)) {
            unlink($installLocation);
        }

        echo 'Linking: ' . $outputFileName . ' => ' . $installLocation . ' ';
        $worked = @symlink($outputFileName, $installLocation);
        echo $worked ? 'Done' : 'Failed';
        echo PHP_EOL;
    }
    else {
        echo 'Already Linked: ' . $outputFileName . ' => ' . $installLocation . PHP_EOL;
    }
}