<?php

$urlPrefix = 'git@gitlab.com:updashd/';

$projects = [
	// Resources
	'updashd-resources',
	'updashd-website',
	
	// Libraries
	'updashd-configlib',
	'updashd-model',
	'updashd-scheduler',
	'updashd-worker-php-interface',
	'updashd-notifier-php-interface',
	
	// Queue Projects
	'updashd-runner',
	'updashd-agent',
	'updashd-rescheduler',
	'updashd-dispatcher',
	'updashd-recorder',
	'updashd-worker-php',
	'updashd-notifier-php',
	
	// Queue and DB Maintenance/Monitoring
	'updashd-monitor',
	'updashd-watchdog',
	'updashd-db-maintenance',

    // Indexing (Elasticsearch)
    'updashd-indexer',
	
	// Queue Plugins
	'updashd-worker-php-mysql',
	'updashd-worker-php-dns',
	'updashd-worker-php-http',
	'updashd-worker-php-icmp-ping',
	
	// Notifiers
	'updashd-notifier-php-slack',
	
	// Front End
	'updashd-webapp',
	'updashd-pusher',
	'updashd-pusher',
	
	// Installer
	'updashd-installer'
];

// If we have arguments given, treat them as a list of specific projects
if ($argc > 1) {
    $subset = array_slice($argv, 1);
    $projects = array_intersect($projects, $subset);
}
else {
    $dirs = glob('*', GLOB_ONLYDIR);

    foreach (array_diff($dirs, $projects) as $project) {
        echo 'Unrecognized: ' . $project . PHP_EOL;
    };
}


if (! count($projects)) {
    echo 'Nothing to do!' . PHP_EOL;
}

$revisit = [];

function printHeading ($text, $lineChar = '=') {
    echo str_repeat($lineChar, strlen($text)) . PHP_EOL;
    echo $text . PHP_EOL;
    echo str_repeat($lineChar, strlen($text)) . PHP_EOL;
}

function printError ($project) {
    printHeading('Error: ' . $project, '#');
}

$originDir = getcwd();

foreach ($projects as $project) {
	chdir($originDir);
	
	echo str_repeat('=', strlen($project)) . PHP_EOL;
	echo $project . PHP_EOL;
	echo str_repeat('=', strlen($project)) . PHP_EOL;

	$retVal = null;
	
	$isNew = false;
	
	if (! file_exists($project)) {
		$isNew = true;
		
		passthru('git clone ' . $urlPrefix . $project . ' ' . $project, $retVal);

		if ($retVal) {
		    printError($project);
		    $revisit[] = $project;
		    continue;
        }
	}
	
	chdir($project);
	
	if (! $isNew) {
        echo 'On Branch: ';

        passthru('git symbolic-ref --short HEAD');

        // Revert composer.lock if it exists
        if (file_exists('composer.lock')) {
            passthru('git checkout composer.lock', $retVal);
        }

		passthru('git pull', $retVal);

        if ($retVal) {
            printError($project);
            $revisit[] = $project;
            continue;
        }
	}
	
	// If this project uses composer
	if (file_exists('composer.json')) {
	    if (file_exists('composer.lock')) {
            passthru('git ls-files composer.lock --error-unmatch', $checkVal);

            $isVersioned = ! $checkVal;

            // Remove lock to force a fresh install, if composer.lock is not versioned
            if ($isVersioned) {
                @unlink('composer.lock');
            }
        }

		// Run the installer
		passthru('composer install', $retVal);

        if ($retVal) {
            printError($project);
            $revisit[] = $project;
            continue;
        }
	}
}

if (count($revisit)) {
    printHeading('Projects with Errors');

    foreach ($revisit as $project) {
        echo $project . PHP_EOL;
    }
}